var App = {
    init: function(){
    	$('.slider-videos').slick({
		  infinite: false,
		  dots: true,
		  arrows: false,
		  dotsClass: 'custom-dots',
		  slidesToShow: 1
		});

    	skrollr.init({
    		smoothScrolling: true
    	});

    	$('#historico').height( $(window).height() - 100 );

        var smartMargin;
        if( $(window).height() <= 780 ){
            smartMargin = .13;
        } else {
            smartMargin = .1;
        }
        $(window).on('resize', function() {
            $('#historico').height( $(window).height() - 100 );
            if( $(window).height() <= 780 ){
                smartMargin = .13;
            } else {
                smartMargin = .1;
            }
        });

    	$('.stickyclass').stick_in_parent();
    	$('#historico').stick_in_parent();

    	$(window).on("scroll", function(){
    		if ($(window).scrollTop() >= 3500) {
    			var margin = ($(window).scrollTop() - 3500) * smartMargin;
    			$('#historico .container').css('margin-top', margin * -1);
    		}
    	});

    }
}

$(function(){
    App.init();
});