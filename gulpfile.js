//////////////////////////////////////////////////
// REQUIRE
//////////////////////////////////////////////////

var gulp       = require('gulp'),
    concat     = require('gulp-concat'),
    uglify     = require('gulp-uglify'),
    prefix     = require('gulp-autoprefixer'),
    less       = require('gulp-less'),
    livereload = require('gulp-livereload'),
    plumber    = require('gulp-plumber');
    watch      = require('gulp-watch');

var onError = function(err) {
    console.log(err.message);
    this.emit('end');
};

//////////////////////////////////////////////////
// PATHS
//////////////////////////////////////////////////

var paths = {
    assets        : 'assets',
    styles        : 'assets/styles',
    scripts       : 'assets/scripts',
    public        : 'app',
    bower         : 'assets/bower_components'
};

//////////////////////////////////////////////////
// CSS Tasks
//////////////////////////////////////////////////

// LESS BUILD
gulp.task('less', function() {
    return gulp.src([
            paths.styles + '/styles.less'
        ])
        .pipe(plumber({errorHandler: onError}))
        .pipe(less({
            paths: [
                paths.bower,
                paths.styles
            ]
        }))
        .pipe(gulp.dest(paths.public + '/css'));
});

// DEVELOPMENT BUILD
gulp.task('css', ['less'], function() {
    return gulp.src([
            paths.public + '/css/styles.css'
        ])
        .pipe(plumber({errorHandler: onError}))
        .pipe(prefix('last 2 versions'))
        .pipe(gulp.dest(paths.public + '/css'));
});

//////////////////////////////////////////////////
// COPY ASSETS
//////////////////////////////////////////////////
gulp.task('assets:copy', function() {
    // FONTS
    gulp.src([
            paths.bower + '/slick-carousel/slick/fonts/**',
        ])
        .pipe(gulp.dest(paths.public + '/fonts'));
});

//////////////////////////////////////////////////
// JAVASCRIPT Tasks
//////////////////////////////////////////////////

// DEVELOPMENT BUILD
gulp.task('js:vendor', function() {
    return gulp.src([
            paths.bower + '/jquery/dist/jquery.js',
            paths.bower + '/slick-carousel/slick/slick.js',
            paths.bower + '/skrollr/dist/skrollr.min.js',
            paths.bower + '/sticky-kit/jquery.sticky-kit.min.js'
        ])
        .pipe(plumber({errorHandler: onError}))
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.public + '/js'));
});

gulp.task('js:dev', function() {
    return gulp.src([
        paths.scripts + '/*.js'
    ])
        .pipe(plumber({errorHandler: onError}))
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(paths.public + '/js'));
});

gulp.task('js:prod', ['js:dev'], function() {
    return gulp.src([
        paths.scripts + '/js/scripts.js'
    ])
        .pipe(plumber({errorHandler: onError}))
        .pipe(uglify())
        .pipe(gulp.dest(paths.public + '/js'));
});

//////////////////////////////////////////////////
// WATCH TASKS
//////////////////////////////////////////////////

gulp.task('watch', function() {
    livereload.listen();

    gulp.watch(paths.styles + '/**/*.less', function(){
        gulp.start('css')
    });

    gulp.watch(paths.scripts + '/**/*.js', function(){
        gulp.start('js:dev')
    });

    gulp.watch([paths.public + '/**'], function(file){
        livereload.changed(file);
    });
});

//////////////////////////////////////////////////
// BUILD TASKS
//////////////////////////////////////////////////

gulp.task('default', ['css', 'js:vendor', 'js:dev', 'watch']);
gulp.task('copy', ['assets:copy']);